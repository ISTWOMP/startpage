#!/bin/bash

if [ "$EUID" -ne 0 ]
  then echo "Please run as root"
  exit
fi

PARENT_FOLDER="/srv/http"
GIT_FOLDER="/srv/http/homepage"

echo "Killing Apt and refreshing time"
pkill -f apt
rm /var/lib/dpkg/lock
systemctl restart systemd-timesyncd.service
sleep 3

echo "Updating via apt"
apt update
apt install nginx php-fpm git

#/homepage?
mkdir $PARENT_FOLDER
cd $PARENT_FOLDER
git clone https://ISTWOMP@bitbucket.org/ISTWOMP/startpage.git homepage
cd $GIT_FOLDER

ln -sf $GIT_FOLDER/homepage_nginx /etc/nginx/sites-available/homepage
ln -sf /etc/nginx/sites-available/homepage /etc/nginx/sites-enabled/homepage

usermod -a -G www-data ubuntu
chgrp -R www-data $GIT_FOLDER
chmod -R g+w $GIT_FOLDER
	
service nginx restart