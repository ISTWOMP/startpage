#!/bin/bash

if [ "$EUID" -ne 0 ]
  then echo "Please run as root"
  exit
fi

certbot --nginx -m thomas.r.waller@gmail.com --agree-tos --no-eff-email --redirect -d homepage.waller.rocks